package com.adriesavana.barcodescan;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.adriesavana.barcodescan.util.IntentResultZxingBarcode;
import com.adriesavana.barcodescan.util.IntentZxingBarcode;

public class MainActivity extends AppCompatActivity {

    private IntentZxingBarcode integrator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        integrator = new IntentZxingBarcode(this);
        Button btnScan = (Button) findViewById(R.id.scanBarcode);

        btnScan.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                integrator.initiateScan();
            }
        });
    }



    /**
     * Dispatch incoming result to the correct fragment.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResultZxingBarcode scanningResult = IntentZxingBarcode.parseActivityResult(requestCode, resultCode, data);
        if (scanningResult != null)
        {
            String barscanresult = scanningResult.getContents();

            Log.i("xyz", barscanresult);
            Toast.makeText(getApplicationContext(), barscanresult, Toast.LENGTH_SHORT).show();
        }
        else
        {
            Toast.makeText(getApplicationContext(), "No scan data received!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
